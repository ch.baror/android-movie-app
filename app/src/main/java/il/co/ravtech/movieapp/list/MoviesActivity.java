package il.co.ravtech.movieapp.list;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;



import java.util.List;

import il.co.ravtech.movieapp.R;
import il.co.ravtech.movieapp.db.AppDatabase;
import il.co.ravtech.movieapp.details.DetailsActivity;
import il.co.ravtech.movieapp.model.MovieListResult;
import il.co.ravtech.movieapp.model.MovieModel;
import il.co.ravtech.movieapp.model.MovieModelConverter;
import il.co.ravtech.movieapp.model.MoviesContent;
import il.co.ravtech.movieapp.rest.MoviesService;
import il.co.ravtech.movieapp.rest.RestClientManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviesActivity extends AppCompatActivity implements OnMovieClickListener {

    private RecyclerView recyclerView;
    private View progressBar;
    private MoviesViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        recyclerView = findViewById(R.id.movies_rv_list);
        progressBar = findViewById(R.id.main_progress);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getCachedMoviesFromDataBase();

        loadMovies();
    }

    private void getCachedMoviesFromDataBase() {

        MoviesContent.clear();
        List<MovieModel> cachedMovies = AppDatabase.getInstance(this).movieDao().getAll();
        MoviesContent.MOVIES.addAll(cachedMovies);
        adapter = new MoviesViewAdapter(MoviesContent.MOVIES, MoviesActivity.this);
        recyclerView.setAdapter(adapter);
        adapter.setData(MoviesContent.MOVIES);
        adapter.notifyDataSetChanged();
    }



    @Override
    public void onMovieClicked(int itemPosition) {
        if (itemPosition < 0 || itemPosition >= MoviesContent.MOVIES.size()) return;

        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.EXTRA_ITEM_POSITION, itemPosition);
        startActivity(intent);
    }

    private void loadMovies() {

        progressBar.setVisibility(View.VISIBLE);
        MoviesService moviesService = RestClientManager.getMovieServiceInstance();
        moviesService.getPopular().enqueue(new Callback<MovieListResult>() {
            @Override
            public void onResponse(@NonNull Call<MovieListResult> call, @NonNull Response<MovieListResult> response) {
                Log.i("response", "response");
                progressBar.setVisibility(View.GONE);
                if (response.code() == 200 && response.body() != null) {
                    MoviesContent.clear();
                    MoviesContent.MOVIES.addAll(MovieModelConverter.convertResult(response.body()));
                    adapter.setData(MoviesContent.MOVIES);
                    AppDatabase.getInstance(MoviesActivity.this).movieDao().deleteAll();
                    AppDatabase.getInstance(MoviesActivity.this).movieDao().insertAll(MoviesContent.MOVIES);
                }
            }

            @Override
            public void onFailure(Call<MovieListResult> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(MoviesActivity.this, R.string.something_went_wrong_text, Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                AppDatabase.getInstance(this.getApplicationContext()).movieDao().deleteAll();
                adapter.clearData();
                break;
        }

        return true;
    }
}
