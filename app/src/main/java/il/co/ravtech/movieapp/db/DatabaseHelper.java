package il.co.ravtech.movieapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.util.ArrayList;

import il.co.ravtech.movieapp.model.MovieResult;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_NAME = "MOVIE_ENTRY";

    // Table columns
    public static final String COLUMN_NAME_ID = "id";
    public static final String COLUMN_NAME_NAME = "name";
    public static final String COLUMN_NAME_CONTENT = "content";
    public static final String COLUMN_NAME_IMAGE_URI = "image_uri";
    public static final String COLUMN_NAME_RELEASE_DATE = "release_date";


    // Database Information
    static final String DB_NAME = "MoviesDb.db";

    // database version
    static final int DB_VERSION = 1;

    // Creating table query
    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + TABLE_NAME + "(" +
            COLUMN_NAME_ID + " INTEGER PRIMARY KEY, " +
            COLUMN_NAME_NAME + " TEXT, " +
            COLUMN_NAME_CONTENT + " TEXT, " +
            COLUMN_NAME_RELEASE_DATE + " TEXT, " +
            COLUMN_NAME_IMAGE_URI + " TEXT);";

    private SQLiteDatabase database;

    public void open() throws SQLException {
        database = this.getWritableDatabase();
    }

    public void close() {
        database.close();
    }

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void saveMovie(MovieResult movie) {
        try {
            open();
            ContentValues contentValue = new ContentValues();
            contentValue.put(DatabaseHelper.COLUMN_NAME_ID, movie.getId());
            contentValue.put(DatabaseHelper.COLUMN_NAME_NAME, movie.getTitle());
            contentValue.put(DatabaseHelper.COLUMN_NAME_CONTENT, movie.getOverview());
            contentValue.put(DatabaseHelper.COLUMN_NAME_IMAGE_URI, movie.getPosterPath());
            contentValue.put(DatabaseHelper.COLUMN_NAME_RELEASE_DATE, movie.getReleaseDate());
            database.insert(TABLE_NAME, null, contentValue);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            close();
        }

    }

    public ArrayList<MovieResult> getAllMovies() {
        open();
        ArrayList<MovieResult> movies = new ArrayList<>();
        String[] columns = new String[]{DatabaseHelper.COLUMN_NAME_ID,
                DatabaseHelper.COLUMN_NAME_NAME,
                DatabaseHelper.COLUMN_NAME_CONTENT,
                DatabaseHelper.COLUMN_NAME_IMAGE_URI,
                DatabaseHelper.COLUMN_NAME_RELEASE_DATE};
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, null,
                null, null, null, null);
        if (cursor.getCount() > 0) {
            int columnIndexId = cursor.getColumnIndexOrThrow(COLUMN_NAME_ID);
            int columnIndexName = cursor.getColumnIndexOrThrow(COLUMN_NAME_NAME);
            int columnIndexContent = cursor.getColumnIndexOrThrow(COLUMN_NAME_CONTENT);
            int columnIndexImageUri = cursor.getColumnIndexOrThrow(COLUMN_NAME_IMAGE_URI);
            int columnIndexReleaseDate = cursor.getColumnIndexOrThrow(COLUMN_NAME_RELEASE_DATE);

            while (cursor.moveToNext()) {

                movies.add(new MovieResult(
                        cursor.getInt(columnIndexId),
                        cursor.getString(columnIndexName),
                        cursor.getString(columnIndexImageUri),
                        cursor.getString(columnIndexContent),
                        cursor.getString(columnIndexReleaseDate)));
            }

        }
        cursor.close();
        close();
        return movies;
    }

    public MovieResult getMovieById(int id) {
        open();
        MovieResult result = null;
        Cursor cursor = database.rawQuery("SELECT * FROM Movies WHERE " + DatabaseHelper.COLUMN_NAME_ID + " = " + id, null);
        if (cursor != null) {

            result = new MovieResult(
                    cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NAME_ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NAME_NAME)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NAME_IMAGE_URI)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NAME_CONTENT)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NAME_RELEASE_DATE)));

            cursor.close();
        }
        close();
        return result;
    }

}